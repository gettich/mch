export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'frontend',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/style.scss',
    '~/node_modules/element-ui/lib/theme-chalk/index.css',
  ],

  loading: false,

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/element-ui', {src: '@/plugins/line-chart', mode: 'client'}],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'nuxt-socket-io'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: `${process.env.BASE_URL}/api`,
  },

  auth: {
    redirect: {
      login: '/login',
      home: '/',
    },
    localStorage: false,
    scopeKey: 'scope',
    strategies: {
      local: {
        token: {
          property: 'accessToken', //property name that the Back-end sends for you as a access token for saving on localStorage and cookie of user browser
          global: true,
          required: true,
          type: 'Bearer',
        },
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            property: 'accessToken',
          },
          user: {
            url: 'user/me',
            method: 'get',
            property: false,
          },
          logout: true,
        },
      },
    },
  },

  io: {
    sockets: [{
      name: 'main',
      url: process.env.BASE_URL,
      default: true,
    }]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
