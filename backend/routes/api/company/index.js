const router = require("express").Router();
const companyController = require("./handlers");

router.post("/all", companyController.getCompanies);
router.get("/count", companyController.getCompaniesCount)
router.get('/stat/:okved', companyController.getOkvedStat)

module.exports = router;
