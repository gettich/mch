const { Company } = require("../../../controllers/companyController");

exports.getCompanies = async (req, res, next) => {
  try {
    var { page, per_page } = req.body;
    page = parseInt(page);
    per_page = Math.min(100, parseInt(per_page));
    const companies = await Company.find({})
      .skip((page - 1) * per_page)
      .limit(per_page);
    res.status(200).json({
      data: companies,
    });
  } catch (error) {
    next(error);
  }
};

exports.getCompaniesCount = async (req, res, next) => {
  const count = await Company.countDocuments();
  res.status(200).json({
    count: count,
  });
};

exports.getOkvedStat = async (req, res, next) => {
  const okved = req.params.okved;
  const regex = new RegExp(`^${okved}.*$`)
  const stat = await Company.aggregate([{ $match: {mainOkved: regex} }, {$group: { _id : null, sum : { $sum: '$profit' }, count:{$sum:1} } }])
  res.status(200).json(stat)
}