const router = require("express").Router();
const { search } = require("../../controllers/companyController")

router.get("/okveds/:query", async (req, res) => {
    const query = req.params.query;
    const results = await search(query, "okveds")
    res.json(results)
})

router.get("/ogrn/:query", async (req, res) => {
    const query = req.params.query;
    const results = await search(query, "ogrn")
    res.json(results)
})

router.get("/:query", async (req, res) => {
    const query = req.params.query;
    const results = await search(query);
    res.json(results)
})

module.exports = router;