const {
  User,
  allowIfLoggedin,
  grantAccess,
} = require("../../../controllers/userController");

exports.allowIfLoggedin = allowIfLoggedin;
exports.grantAccess = grantAccess;

exports.getUsers = async (req, res, next) => {
  const users = await User.find({}).select(['-password', '-accessToken']);
  res.status(200).json(users);
};

exports.getUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    const user = await User.findById(userId);
    if (!user) return next(new Error("User does not exist"));
    res.status(200).json({
      data: user,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    const update = req.body;
    const userId = req.params.userId;
    console.log(update, userId);
    await User.findByIdAndUpdate(userId, update);
    const user = await User.findById(userId);
    res.status(200).json({
      data: user,
      message: "User has been updated",
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    await User.findByIdAndDelete(userId);
    res.status(200).json({
      data: null,
      message: "User has been deleted",
    });
  } catch (error) {
    next(error);
  }
};

exports.me = async (req, res, next) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId);
    if (!user) return next(new Error("User does not exist"));
    res.status(200).json({
      user: { id: user._id,email: user.email, role: user.role, company: user.company },
    });
  } catch (error) {
    next(error);
  }
};

exports.getRoles = async (req, res, next) => {
  const {rolesEnum} = require("../../../roles")
  res.status(200).json({
    roles: rolesEnum
  })
}
