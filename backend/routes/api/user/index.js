const router = require("express").Router();

const userController = require("./handlers");

router.get("/me", userController.allowIfLoggedin, userController.me);

router.get(
  "/all",
  userController.allowIfLoggedin,
  userController.grantAccess("readAny", "profile"),
  userController.getUsers
);

router.get("/roles", userController.allowIfLoggedin, userController.getRoles);

router.get("/:userId", userController.allowIfLoggedin, userController.getUser);

router.put(
  "/:userId",
  userController.allowIfLoggedin,
  userController.grantAccess("updateOwn", "profile"),
  userController.updateUser
);

router.delete(
  "/:userId",
  userController.allowIfLoggedin,
  userController.grantAccess("deleteAny", "profile"),
  userController.deleteUser
);

module.exports = router;
