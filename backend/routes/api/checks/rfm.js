const router = require("express").Router();
const https = require("https");
const querystring = require("querystring");

router.get("/", (req, res) => {
  const name = req.query.name;

  const parameters = {
    query: name,
  };

  const reqargs = querystring.stringify(parameters);
  const options = {
    hostname: "fedsfm.ru",
    port: "443",
    path: "/TerroristSearchAutocomplete?" + reqargs,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    rejectUnauthorized: false,
  };

  const request = https.request(options, (response) => {
    response.on("data", function (chunk) {
      res.send(String(chunk));
    });
  });

  request.on("error", (error) => {
    res.send("{}");
  });

  request.end();
});

module.exports = router;
