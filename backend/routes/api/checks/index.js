const router = require("express").Router();
router.use("/rfm", require("./rfm"));
router.use("/offshores", require("./offshores"));
module.exports = router;
