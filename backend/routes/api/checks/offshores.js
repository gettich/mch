const router = require("express").Router();
const https = require('https');
const querystring = require('querystring');

router.get(
    "/",
    (req, res) => {
        const query = req.query.query;

        const parameters = {
            c: 'RUS',
            q: query,
            d: '',
            j: ''
        }
    
        const reqargs = querystring.stringify(parameters);
        const options = {
            hostname: "offshoreleaks.icij.org",
            port: "443",
            path: "/search?" + reqargs,
            method: 'GET',
            headers: {'User-Agent' : req.headers["user-agent"] }
        }
        
        const request = https.request(options, (response) => {
            let chunks = [];

            response.on('data', function (chunk) {
                chunks.push(chunk);               
            });

            response.on('end', () => {
                var str = String(chunks);
                request.end();
                if(str.includes("No results found"))
                    res.send("{}");
                else
                    res.send('https://' + options.hostname + options.path);
            } );

            response.on('error', (error) => {
                res.send('{}');             
            });
        });

        request.on('error', (error) => {
            res.send('{}');
        });

        request.end();
    }
  );

module.exports = router;