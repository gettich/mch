const { Product } = require("../../../controllers/productController");

exports.getProducts = async (req, res, next) => {
  const products = await Product.find({});
  res.status(200).json(products);
};

exports.search = async (req, res, next) => {
  const query = req.params.query;
  const results = await Product.find({ $text: { $search: query } }).limit(100);
  res.json(results);
};
