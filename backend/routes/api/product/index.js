const router = require("express").Router();
const productController = require('./handlers');

router.get('/', productController.getProducts)
router.get('/search/:query', productController.search)

module.exports = router