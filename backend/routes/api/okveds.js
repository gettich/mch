const router = require("express").Router();
const { Okveds } = require("../../controllers/okvedsController");

router.get("/", async (_req, res) => {
  const okveds = await Okveds.find({}).select(["-_id"]);
  return res.json(okveds);
});

router.get("/sectors", async (_req, res) => {
  const okveds = await Okveds.find({ code: /^[0-9]{2}$/ }).select(["-_id"]);
  return res.json(okveds);
});

module.exports = router;
