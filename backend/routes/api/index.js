const router = require("express").Router();
const jwt = require("jsonwebtoken");
const User = require("../../models/userModel");
const { route } = require("./auth");

router.use(async (req, res, next) => {
  if (req.headers["authorization"]) {
    const authHeader = req.headers["authorization"];
    const accessToken = authHeader.split("Bearer").join("").trim();
    const { userId, exp } = await jwt.verify(
      accessToken,
      process.env.JWT_SECRET
    );
    // Check if token has expired
    if (exp < Date.now().valueOf() / 1000) {
      return res.status(401).json({
        error: "JWT token has expired, please login to obtain a new one",
      });
    }
    res.locals.loggedInUser = await User.findById(userId);
    next();
  } else {
    next();
  }
});

router.use("/auth", require("./auth"));
router.use("/user", require("./user"));
router.use("/search", require("./search"));
router.use("/checks", require("./checks"));
router.use("/company", require("./company"));
router.use("/okveds", require("./okveds"));
router.use("/product", require("./product"));

module.exports = router;
