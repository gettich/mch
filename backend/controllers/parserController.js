const axios = require("axios").default;
const cheerio = require("cheerio");
const { Company } = require("./companyController");

console.log("Parser loop started");
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
const pauseTime = 1 * 1000;

async function parseLicense(ogrn) {
  const url = `https://vbankcenter.ru/contragent/${ogrn}/licenses`;
  const response = await axios.get(url);
  const $ = cheerio.load(response.data);
  const licenses_html = $(".pageable-content").html();
  const number_re = /(№[^<]*)/gm;
  const date_re = /(от [0-9]+.[0-9]+.[0-9]+)/gm;
  const by_re = /Лицензирующий орган:<\/span>[^<]*<span>([^<]*)<\/span>/gm;
  const numbers = licenses_html.match(number_re);
  const dates = licenses_html.match(date_re);
  const byes = licenses_html.match(by_re);

  const licenses = [];
  if (!numbers) return licenses;
  for (let ix = 0; ix < numbers.length; ix++) {
    if (byes[ix])
      byes[ix] = byes[ix]
        .replace(/<\/?[^>]*>/g, "")
        .replace(/&[^;]*;/g, "")
        .replace("Лицензирующий орган:", "")
        .replace(/\s{2,}/g, " ")
        .trim();
    licenses.push({
      number: numbers[ix],
      from: dates[ix] || "-",
      by: byes[ix] || "-",
    });
  }

  return licenses;
}

async function parseGeneral(ogrn) {
  const url = `https://vbankcenter.ru/contragent/${ogrn}`;
  const response = await axios.get(url);
  const $ = cheerio.load(response.data);
  const rating_html = $("gweb-reliability-widget")
    .text()
    .match(/[0-9]+/g);
  const rating = rating_html && rating_html[0] ? parseInt(rating_html[0]) : 0;
  const profit_html = $("gweb-finanstat-item:nth-child(3)")
    .text()
    .match(/\((-?[0-9]+,?[0-9]+)%\)/);
  const profit = profit_html ? parseFloat(profit_html[1].replace(",", ".")) : 0;

  const pre_mainOkved = $("gweb-requisites-ul")
    .html()
    .match(/contragent\/codes\/[0-9]*">([^<]+)/m);
  const mainOkved_html =
    pre_mainOkved && pre_mainOkved[1] ? pre_mainOkved[1].match(/[0-9\.]+/) : "";
  const mainOkved =
    mainOkved_html && mainOkved_html[0] ? mainOkved_html[0] : "";

  return { mainOkved, rating, profit };
}

async function parse() {
  while (true) {
    const count = await Company.countDocuments();
    const per_page = 100;
    const pages = Math.ceil(count / per_page);
    for (let page = 0; page < pages; page++) {
      const companies = await Company.find({})
        .skip(page * per_page)
        .limit(per_page);

      for (let ix = 0; ix < companies.length; ix++) {
        const company = companies[ix];
        try {
          if (company.profit == 0.1 || company.mainOkved == "") {
            const general = await parseGeneral(company.ogrn);
            company.rating = general.rating;
            company.profit = general.profit;
            company.mainOkved = general.mainOkved;
          }
          const licenseCount = company.licenses.length;
          if (licenseCount == 0)
            company.licenses = await parseLicense(company.ogrn);
          await Company.findByIdAndUpdate(company._id, company);
        } catch (error) {
          console.log(error);
        }
      }
    }
    await delay(pauseTime);
  }
}

parse();
