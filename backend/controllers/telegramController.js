const { Telegraf } = require("telegraf");

const bot = new Telegraf(process.env.BOT_TOKEN);
const chats = [process.env.CHAT_ID];
bot.on("channel_post", (ctx) => {
  const post = ctx.channelPost;
  if (chats.includes(post.chat.id.toString())) {
    global.io.local.emit("notifyEvent", post);
  }
});

bot.launch().then(() => {
  console.log("Telegram analyzer started");
});
