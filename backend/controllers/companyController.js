const Company = require("../models/companyModel");

exports.Company = Company;

exports.search = async (query, type = "all") => {
  try {
    if (type == "okveds") {
      const q = query.split(",");
      const companies = await Company.find({ okveds: { $all: q } }).limit(100);
      return companies;
    } else if (type == "ogrn") {
      const company = await Company.findOne({ ogrn: query });
      return company;
    } else {
      const companies = await Company.find({ $text: { $search: query } }).limit(
        100
      );
      return companies;
    }
  } catch (error) {
    console.log(error);
  }
};

exports.removeDuplicates = async () => {
  try {
    Company.aggregate(
      [
        {
          $group: {
            _id: { ogrn: "$ogrn" },
            slugs: { $addToSet: "$_id" },
            count: { $sum: 1 },
          },
        },
        {
          $match: {
            count: { $gt: 1 },
          },
        },
      ],
      function (err, docs) {
        docs.forEach((doc) => {
          doc.slugs.shift();
          doc.slugs.forEach((slug) => {
            Company.findByIdAndDelete({ _id: slug }, function (err) {
              if (err) console.log(err);
            });
          });
        });
      }
    );
  } catch (error) {
    console.log(error);
  }
};
