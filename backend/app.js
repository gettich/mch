require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");

const app = express();
app.use(cors());
const listener = require("http").createServer(app);
global.io = require("socket.io")(listener, {
  cors: {
    origin: "*",
    methods: ["GET", "POST", "OPTIONS"]
  }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

app.disable("x-powered-by");

mongoose.connect("mongodb://localhost:27017/mch").then(() => {
  console.log("Connected to the database successfully");
});

require("./controllers/wsController");
require("./controllers/telegramController");
require("./controllers/parserController")

app.use(require("./routes"));

const port = process.env.PORT || 3333;
global.server = listener.listen(port, () => {
  console.log("Listening on port " + server.address().port);
});
