import json
import pandas as pd
import numpy as np
import urllib

from pyspark.ml import Pipeline
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from sparknlp.annotator import *
from sparknlp.base import *
import sparknlp
from sparknlp.pretrained import PretrainedPipeline
from http.server import BaseHTTPRequestHandler, HTTPServer

nlp_pipeline = None
spark = None

class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        request = urllib.parse.parse_qs(self.path)
        company = main(request['/?query'], nlp_pipeline, spark)
        self.wfile.write(bytes(company, "utf8"))

def init():
    spark = sparknlp.start()

    MODEL_NAME = "wikiner_6B_100"

    document_assembler = DocumentAssembler() \
        .setInputCol('text') \
        .setOutputCol('document')

    tokenizer = Tokenizer() \
        .setInputCols(['document']) \
        .setOutputCol('token')

    if MODEL_NAME == "wikiner_840B_300":
        embeddings = WordEmbeddingsModel.pretrained('glove_840B_300', lang='xx') \
            .setInputCols(['document', 'token']) \
            .setOutputCol('embeddings')
    elif MODEL_NAME == "wikiner_6B_300":
        embeddings = WordEmbeddingsModel.pretrained('glove_6B_300', lang='xx') \
            .setInputCols(['document', 'token']) \
            .setOutputCol('embeddings')
    elif MODEL_NAME == "wikiner_6B_100":
        embeddings = WordEmbeddingsModel.pretrained('glove_100d') \
            .setInputCols(['document', 'token']) \
            .setOutputCol('embeddings')

    ner_model = NerDLModel.pretrained(MODEL_NAME, 'ru') \
        .setInputCols(['document', 'token', 'embeddings']) \
        .setOutputCol('ner')

    ner_converter = NerConverter() \
        .setInputCols(['document', 'token', 'ner']) \
        .setOutputCol('ner_chunk')

    nlp_pipeline = Pipeline(stages=[
        document_assembler, 
        tokenizer,
        embeddings,
        ner_model,
        ner_converter
    ])
    return spark, nlp_pipeline

def main(text, nlp_pipeline, spark):
    text_list = text

    empty_df = spark.createDataFrame([['']]).toDF('text')
    pipeline_model = nlp_pipeline.fit(empty_df)
    df = spark.createDataFrame(pd.DataFrame({'text': text_list}))
    result = pipeline_model.transform(df)

    for res in result.select(F.collect_list('ner')).first():
        for r in res:
            for out in r:
                if(out["result"] == 'B-ORG'):
                    return out["metadata"]["word"]
                    
if __name__ == '__main__':  
    spark, nlp_pipeline = init()
    hostName = "localhost"
    serverPort = 8080
    webServer = HTTPServer((hostName, serverPort), Server)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()