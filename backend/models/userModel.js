const mongoose = require('mongoose');
const Schema = mongoose.Schema;
 
const UserSchema = new Schema({
 email: {
  type: String,
  required: true,
  trim: true
 },
 password: {
  type: String,
  required: true
 },
 role: {
  type: String,
  default: 'basic',
  enum: ["basic", "employee", "company", "supervisor", "admin"]
 },
 accessToken: {
  type: String
 },
 company: {
    type: mongoose.Types.ObjectId
 }
});
 
const User = mongoose.model('user', UserSchema);
 
module.exports = User;