const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CompanySchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  ogrn: {
    type: Number,
    required: true,
    trim: true,
    index: true,
  },
  inn: {
    type: Number,
    required: true,
    trim: true,
  },
  managers: {
    type: [String],
    required: false,
  },
  founders: [{ name: String, legal: Boolean, foreign: Boolean }],
  okveds: {
    type: [String],
    required: true,
    index: true,
  },
  mainOkved: { type: String, required: true },
  contacts: {
    mail: [String],
    phone: [String],
    address: [String],
    website: [String],
  },
  licenses: [
    {
      number: String,
      from: String,
      by: String,
    },
  ],
  profit: { type: Number, required: true },
  rating: { type: Number, required: true },
  regDate: {
    type: String,
  },
});

CompanySchema.index({ "$**": "text" });

const Company = mongoose.model("company", CompanySchema);

module.exports = Company;
