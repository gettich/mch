const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  manufacturer: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  img: {
    type: String,
    required: true
  },
  pending: {
    type: Boolean,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
});

ProductSchema.index({ "$**": "text" });

const Product = mongoose.model("product", ProductSchema);

module.exports = Product;
