const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OkvedsSchema = new Schema({
  sector:{
    type: String,
    required: true,
    trim: true
  },
  code: {
    type: String,
    required: true,
    trim: true,
  },
  name: {
    type: String,
    required: true,
    trim: true,
  },
});

const Okveds = mongoose.model("okveds", OkvedsSchema);

module.exports = Okveds;
