const AccessControl = require("accesscontrol");
const ac = new AccessControl();

const rolesEnum = {
  basic: "basic",
  employee: "employee",
  company: "company",
  supervisor: "supervisor",
  admin: "admin",
};

exports.rolesEnum = rolesEnum;

exports.roles = (function () {
  ac.grant(rolesEnum.basic).readOwn("profile").updateOwn("profile");
  ac.grant(rolesEnum.employee).extend("basic");
  ac.grant(rolesEnum.company).extend("basic");

  ac.grant(rolesEnum.supervisor)
    .extend(rolesEnum.basic)
    .readAny("profile")
    .updateAny("profile");

  ac.grant(rolesEnum.admin)
    .extend(rolesEnum.basic)
    .extend(rolesEnum.supervisor)
    .deleteAny("profile");

  return ac;
})();
